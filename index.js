window.addEventListener('DOMContentLoaded', () => (window.cropper = new Cropper(document.querySelector('#image'), {
  guides: false,
  center: false,
  restore: false,
  rotate: 20,
  dragMode: 'move',
  zoomable: true,
  scalable: true,
  highlight: true,
  rotatable: true,
  background: false,
  responsive: true,
  aspectRatio: 16 / 9,
  autoCropArea: 0.65,
  cropBoxMovable: false,
  cropBoxResizable: true,
  toggleDragModeOnDblclick: false,
})));

const toDataURL = url => fetch(url, { credentials: 'same-origin' })
  .then(response => response.blob())
  .then(blob => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.onerror = reject;
    reader.readAsDataURL(blob)
  }));

const cropImage = callback => {
  const img = document.getElementById('image');
  const imageOptions = {
    croppedImgData: window.cropper.getData(),
    imageData: window.cropper.getImageData(),
  };
  
  console.log('cropped area data', imageOptions.croppedImgData);
  console.log('cropped image data', imageOptions.imageData);
  
  if (img && img.src) {
    toDataURL(img.src).then(value => typeof callback === 'function' && callback(value, imageOptions));
  } else {
    console.log('No img with such ID or img with no src');
  }
};

const rotate = () => window.cropper.rotate(20);

